//
//  GameViewController.swift
//  Mouse
//
//  Created by iosakademija on 7/6/16.
//  Copyright (c) 2016 iosakademija. All rights reserved.
//

import UIKit
import SpriteKit


class GameViewController: UIViewController{
 

    
    var level = 0


    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        func showAlert (){
            
            let ac = UIAlertController(title: "Dragg mouse", message: nil, preferredStyle: .Alert)
            ac.addAction(UIAlertAction(title: "Continue", style: .Default, handler: nil))
            self.presentViewController(ac, animated: true, completion: nil)
        }
        
        

       let NSNotification = NSNotificationCenter.defaultCenter()
        NSNotification.addObserver(self, selector:Selector(showAlert()), name: "showAlert", object: Level1())

        
        if let scene = SKScene.sceneWithClassNamed("Level\(level)", fileNamed: "GameScene"){
            
            // Configure the view.
            let skView = self.view as! SKView
            skView.showsFPS = true
            skView.showsNodeCount = true
            
            /* Sprite Kit applies additional optimizations to improve rendering performance */
            skView.ignoresSiblingOrder = true
            
            
            /* Set the scale mode to scale to fit the window */
            scene.scaleMode = .AspectFill

            skView.presentScene(scene)
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
        
        navigationController?.navigationBarHidden = true
    }

    override func shouldAutorotate() -> Bool {
        return true
    }

    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        if UIDevice.currentDevice().userInterfaceIdiom == .Phone {
            return .AllButUpsideDown
        } else {
            return .All
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Release any cached data, images, etc that aren't in use.
    }

    override func prefersStatusBarHidden() -> Bool {
        return true
    }
    override func viewWillDisappear(animated: Bool) {
        super.viewDidDisappear(true)
        navigationController?.navigationBarHidden = false
    }
}
