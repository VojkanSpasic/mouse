//
//  Level.swift
//  Mouse
//
//  Created by iosakademija on 7/15/16.
//  Copyright © 2016 iosakademija. All rights reserved.
//

import Foundation
import SpriteKit

protocol MouseDelegate{
    
    func updateLevel(level:Int)
    func updateMoves(moves:Int)
}
class Level:SKScene,SKPhysicsContactDelegate {
    
    var level = 1{
        
        didSet{
            levelLabel.text = "Level\(level)"
        }
    }
    var moves:Int = 0{
        
        didSet{
            moveLabel!.text = "moves : \(moves)"
        }
    }

    enum PhysicsCategory:UInt32{
        
        case Wall = 1
        case Mouse = 2
        case Box = 3
    }

    
    var timeLabel:SKLabelNode!
    var moveLabel:SKLabelNode!
    var levelLabel:SKLabelNode!
    var wallFloor :SKSpriteNode!
    var wallCeiling :SKSpriteNode!
    var wallLeft:SKSpriteNode!
    var wallRight :SKSpriteNode!
    
    
    var spritePositionX:CGFloat?
    var spritePositionY:CGFloat?
    var touchedSprite:SKSpriteNode?
    var second = 0
    var minute = 0
    var hour = 0

    
    var mouseDelegate:MouseDelegate?
    var levelsDelegate:LevelsDelegate?
    
    let dragSound = SKAction.playSoundFileNamed("drag_stone", waitForCompletion: false)
    let mouseSound = SKAction.playSoundFileNamed("mice 1", waitForCompletion: false)
    
    override func didMoveToView(view: SKView) {
        super.didMoveToView(view)
        
        
        self.scene?.size = (self.view?.bounds.size)!
        backgroundColor = UIColor.darkGrayColor()
        self.physicsWorld.contactDelegate = self
        self.physicsWorld.gravity = CGVectorMake(0, 0)
        
    
        
//        let gestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(self.handlePanFrom(_:)))
//        self.view!.addGestureRecognizer(gestureRecognizer)
        
        
        
        timeLabel = SKLabelNode(fontNamed: "Chalkduster")
        timeLabel!.fontSize = 20
        timeLabel.fontColor = SKColor.yellowColor()
        timeLabel.position = CGPoint(x: frame.size.width/2, y: frame.size.height - 70)
        timeLabel.text = "Time \(hour):\(minute):\(second)"
        timeLabel.zPosition = 1
        
        addChild(timeLabel!)

        
        moveLabel = SKLabelNode(fontNamed: "Chalkduster")
        moveLabel!.fontSize = 20
        moves = 0
        moveLabel!.fontColor = SKColor.yellowColor()
        moveLabel.position = CGPoint(x: frame.size.width/2, y: frame.size.height - 100)
        moveLabel.zPosition = 1
        addChild(moveLabel!)
        
        levelLabel = SKLabelNode(fontNamed: "Chalkduster")
        levelLabel!.fontSize = 20
        levelLabel.fontColor = UIColor.yellowColor()
        levelLabel.position = CGPoint(x: frame.size.width/2, y: frame.size.height - 130)
        levelLabel.text = "Level1"
        levelLabel.zPosition = 1
        addChild(levelLabel)
        
        loadLevel()
        
        //dimensions
        let playRoomHeight = wallCeiling.position.y - wallFloor.position.y - wallCeiling.size.height
        let playRoomWidth = wallRight.position.x - wallLeft.position.x - wallRight.size.width
        let yDistance = playRoomHeight/6
        let xDistance = playRoomWidth/6
        
        
        let updateTimeLabel = SKAction.runBlock(updateTime)
        let update = SKAction.waitForDuration(1.0)
        let updateSequence = SKAction.sequence([updateTimeLabel,update])
            runAction((SKAction.repeatActionForever(updateSequence)),withKey: "Time")
        
    }
    
    func loadLevel(){
        
        //wallFloor
        
        wallFloor = SKSpriteNode(color: UIColor.blackColor(), size: CGSize(width: (frame.size.width)-40, height: 1))
        wallFloor.physicsBody = SKPhysicsBody(rectangleOfSize: CGSize(width: wallFloor.size.width, height: wallFloor.size.height))
        wallFloor.physicsBody?.dynamic = false
        wallFloor.physicsBody?.allowsRotation = false
        wallFloor.physicsBody?.affectedByGravity = false
        wallFloor.position = CGPoint(x: frame.width/2, y: 120)
        wallFloor.physicsBody?.categoryBitMask = PhysicsCategory.Wall.rawValue
        wallFloor.zPosition = 1
        addChild(wallFloor)
        // wallCeiling
        
        wallCeiling = SKSpriteNode(color: UIColor.blackColor(), size: CGSize(width: (frame.size.width)-40, height: 1))
        wallCeiling.physicsBody = SKPhysicsBody(rectangleOfSize: CGSize(width: wallFloor.size.width, height: wallFloor.size.height))
        wallCeiling.physicsBody?.dynamic = false
        wallCeiling.physicsBody?.allowsRotation = false
        wallCeiling.physicsBody?.affectedByGravity = false
        wallCeiling.position = CGPoint(x: frame.width/2, y: frame.height/4*3)
        wallCeiling.physicsBody?.categoryBitMask = PhysicsCategory.Wall.rawValue
        wallCeiling.physicsBody?.contactTestBitMask = PhysicsCategory.Box.rawValue
        wallCeiling.zPosition = 1
        addChild(wallCeiling)
        
        //wallLeft
        
        wallLeft = SKSpriteNode(color: UIColor.blackColor(), size: CGSize(width: 1, height: wallCeiling.position.y - wallFloor.position.y - 1))
        wallLeft.physicsBody = SKPhysicsBody(rectangleOfSize: wallLeft.size)
        wallLeft.physicsBody?.dynamic = false
        wallLeft.physicsBody?.allowsRotation = false
        wallLeft.physicsBody?.affectedByGravity = false
        wallLeft.position = CGPoint(x: 20, y: (wallCeiling.position.y+wallFloor.position.y)/2)
        wallLeft.physicsBody?.contactTestBitMask = 0
        wallLeft.physicsBody?.categoryBitMask = PhysicsCategory.Wall.rawValue
        wallLeft.physicsBody?.contactTestBitMask = PhysicsCategory.Box.rawValue
        wallLeft.zPosition = 1
        addChild(wallLeft)
        //wallRight
        
        wallRight = SKSpriteNode(color: UIColor.blackColor(), size: CGSize(width: 1, height: wallCeiling.position.y - wallFloor.position.y - 1))
        wallRight.physicsBody = SKPhysicsBody(rectangleOfSize: wallLeft.size)
        wallRight.physicsBody?.dynamic = false
        wallRight.physicsBody?.allowsRotation = false
        wallRight.physicsBody?.affectedByGravity = false
        wallRight.position = CGPoint(x: frame.size.width-20, y: wallLeft.position.y)
        wallRight.name = "wallRight"
        wallRight.physicsBody?.categoryBitMask = PhysicsCategory.Wall.rawValue
        wallRight.physicsBody?.contactTestBitMask = PhysicsCategory.Mouse.rawValue
        wallRight.zPosition = 1
        
        addChild(wallRight)
        
        
    }
    func addBigBoxVerticalWith(position position:CGPoint,playRoomWidth:CGFloat,playRoomHeight:CGFloat)->SKSpriteNode{
        
        let bigBox = SKSpriteNode(imageNamed: "box")
        bigBox.size = CGSize(width:playRoomWidth/6-2, height: playRoomHeight/2-2)
        bigBox.position = position
        bigBox.physicsBody = SKPhysicsBody(rectangleOfSize: CGSize(width: playRoomWidth/6, height: playRoomHeight/2))
        bigBox.physicsBody?.dynamic = false
        bigBox.physicsBody!.affectedByGravity = false
        bigBox.physicsBody?.allowsRotation = false
        bigBox.physicsBody?.restitution = 0
        bigBox.physicsBody?.usesPreciseCollisionDetection = true
        bigBox.physicsBody!.categoryBitMask = PhysicsCategory.Box.rawValue
        bigBox.physicsBody?.contactTestBitMask = PhysicsCategory.Wall.rawValue & PhysicsCategory.Box.rawValue & PhysicsCategory.Mouse.rawValue
        bigBox.name = "bigBoxVertical"
        bigBox.zPosition = 1
        return bigBox
        
    }
    func addMouse(position position:CGPoint,playRoomWidth:CGFloat,playRoomHeight:CGFloat)->SKSpriteNode{
        
        let mouse = SKSpriteNode(imageNamed: "mouse")
        mouse.size = CGSize(width:playRoomWidth/3-2, height: playRoomHeight/6-2)
        mouse.position = position
        mouse.physicsBody = SKPhysicsBody(rectangleOfSize: CGSize(width: playRoomWidth/3, height: playRoomHeight/6))
        mouse.physicsBody?.dynamic = false
        mouse.physicsBody!.affectedByGravity = false
        mouse.physicsBody?.allowsRotation = false
        mouse.physicsBody?.restitution = 0
        mouse.physicsBody?.mass = 0
        mouse.physicsBody?.density = 0
        mouse.physicsBody?.friction = 0
        mouse.physicsBody?.usesPreciseCollisionDetection = true
        mouse.physicsBody?.categoryBitMask = PhysicsCategory.Mouse.rawValue
        mouse.physicsBody?.contactTestBitMask = PhysicsCategory.Wall.rawValue | PhysicsCategory.Box.rawValue | PhysicsCategory.Wall.rawValue
        mouse.name = "mouse"
        mouse.zPosition = 1
        return mouse
        
        
    }
    
    func addMiddleBoxVertical(position position:CGPoint,playRoomWidth:CGFloat,playRoomHeight:CGFloat)->SKSpriteNode{
        
        let box = SKSpriteNode(imageNamed: "box")
        box.size = CGSize(width:playRoomWidth/6-2, height: playRoomHeight/3-2)
        box.position = position
        box.physicsBody = SKPhysicsBody(rectangleOfSize: CGSize(width: playRoomWidth/6, height: playRoomHeight/3))
        box.physicsBody?.dynamic = false
        box.physicsBody!.affectedByGravity = false
        box.physicsBody?.allowsRotation = false
        box.physicsBody?.restitution = 0
        box.physicsBody?.usesPreciseCollisionDetection = true
        box.physicsBody!.categoryBitMask = PhysicsCategory.Box.rawValue
        box.physicsBody?.contactTestBitMask = PhysicsCategory.Wall.rawValue & PhysicsCategory.Mouse.rawValue & PhysicsCategory.Box.rawValue
        box.name = "boxMiddleVertical"
        box.zPosition = 1
        return box
        
    }
    func addMiddleBoxHorizontal(position position:CGPoint,playRoomWidth:CGFloat,playRoomHeight:CGFloat)->SKSpriteNode{
    
    let box = SKSpriteNode(imageNamed: "box")
    box.size = CGSize(width:playRoomWidth/3-2, height: playRoomHeight/6-2)
    box.position = position
    box.physicsBody = SKPhysicsBody(rectangleOfSize: CGSize(width: playRoomWidth/3, height: playRoomHeight/6))
    box.physicsBody?.dynamic = false
    box.physicsBody!.affectedByGravity = false
    box.physicsBody?.allowsRotation = false
    box.physicsBody?.restitution = 0
    box.physicsBody?.usesPreciseCollisionDetection = true
    box.physicsBody!.categoryBitMask = PhysicsCategory.Box.rawValue
    box.physicsBody?.contactTestBitMask = PhysicsCategory.Wall.rawValue & PhysicsCategory.Mouse.rawValue & PhysicsCategory.Box.rawValue
    box.name = "middleBoxHorizontal"
    box.zPosition = 1
    return box
    
    }
    func addBigBoxHorizontal(position position:CGPoint,playRoomWidth:CGFloat,playRoomHeight:CGFloat)->SKSpriteNode{
        
        let box = SKSpriteNode(imageNamed: "box")
        box.size = CGSize(width:playRoomWidth/2-2, height: playRoomHeight/6-2)
        box.position = position
        box.physicsBody = SKPhysicsBody(rectangleOfSize: CGSize(width: playRoomWidth/2, height: playRoomHeight/6))
        box.physicsBody?.dynamic = false
        box.physicsBody!.affectedByGravity = false
        box.physicsBody?.allowsRotation = false
        box.physicsBody?.restitution = 0
        box.physicsBody?.usesPreciseCollisionDetection = true
        box.physicsBody!.categoryBitMask = PhysicsCategory.Box.rawValue
        box.physicsBody?.contactTestBitMask = PhysicsCategory.Wall.rawValue & PhysicsCategory.Mouse.rawValue & PhysicsCategory.Box.rawValue
        box.name = "bigBoxHorizontal"
        box.zPosition = 1
        return box
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        
        super.touchesBegan(touches, withEvent: event)
        let touch = touches.first
        let touchLocation = touch?.locationInNode(self)
        if let body = physicsWorld.bodyAtPoint(touchLocation!) {
            if body.node?.physicsBody?.categoryBitMask == PhysicsCategory.Wall.rawValue{return}
            guard let sprite = body.node as? SKSpriteNode else {return}
            touchedSprite = sprite
            touchedSprite?.physicsBody = sprite.physicsBody
            touchedSprite!.physicsBody?.dynamic = true
            spritePositionX = touchedSprite!.position.x
            spritePositionY = touchedSprite!.position.y
            
        }
    }
    
    
    override func touchesMoved(touches: Set<UITouch>, withEvent event: UIEvent?) {
        super.touchesMoved(touches, withEvent: event)

        for touch in touches{
            
       
            let location = touch.locationInNode(self)
            let previousLocation = touch.previousLocationInNode(self)
//        
//            
//            if let body = physicsWorld.bodyAtPoint(location){
//                guard let sprite = body.node as? SKSpriteNode else{return}

                if touchedSprite?.physicsBody?.dynamic == true{
                    if touchedSprite!.size.height > touchedSprite!.size.width{
                    
                        var spritePositionY = touchedSprite!.position.y + location.y - previousLocation.y
                        
                        spritePositionY = max(spritePositionY, wallFloor.position.y+touchedSprite!.size.height/2)
                        spritePositionY = min(spritePositionY, wallCeiling.position.y - touchedSprite!.size.height/2)
                        touchedSprite!.position.y = spritePositionY
                        
                    }else{
                      
                        var spritePositionX = touchedSprite!.position.x + location.x - previousLocation.x
                        spritePositionX = max(spritePositionX, wallLeft.position.x+touchedSprite!.size.width/2)
                        spritePositionX = min(spritePositionX, wallRight.position.x - touchedSprite!.size.width/2)
                        
                        touchedSprite!.position.x = spritePositionX
                    }
//                }
            }
         }
    }
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
        super.touchesEnded(touches, withEvent: event)
        
        
//        stopDragging()
        if let sprite = touchedSprite{
                if sprite.physicsBody?.categoryBitMask == PhysicsCategory.Wall.rawValue{return}
                if sprite.size.height>sprite.size.width{
                    moveVerticaly(sprite, wallCeiling: wallCeiling, wallFloor: wallFloor)
   
                }else{
                    moveHorizontally(sprite, wallLeft: wallLeft, wallRight: wallRight)
                    
                }
            touchedSprite = nil
        }

    }
    override func touchesCancelled(touches: Set<UITouch>?, withEvent event: UIEvent?) {
        
        touchedSprite = nil
        
    }
    
    
    
    func didBeginContact(contact: SKPhysicsContact) {
        
        if contact.bodyA.categoryBitMask == PhysicsCategory.Mouse.rawValue && contact.bodyB.node!.name == "wallRight"{
            presentGameOverScene()
        }
        if contact.bodyB.categoryBitMask == PhysicsCategory.Mouse.rawValue && contact.bodyA.node!.name == "wallRight"{
            presentGameOverScene()
        }
        
        
    }
  

    
    func presentGameOverScene(){
        
        
        if let gameOverScene = GameOverScene(fileNamed: "GameScene"){
            
            mouseDelegate = gameOverScene
            levelsDelegate = LevelPickerController()
            self.levelsDelegate?.updateLevel(level)
            self.mouseDelegate?.updateLevel(level)
            self.mouseDelegate?.updateMoves(moves)
            let reveal = SKTransition.flipHorizontalWithDuration(0.5)
            gameOverScene.size = (self.view?.bounds.size)!
            self.view?.presentScene(gameOverScene,transition:reveal)
            
        }
        
    }
    
    
    override func update(currentTime: CFTimeInterval) {
        
        
        
    }
    
    
    func moveHorizontally(object:SKSpriteNode,wallLeft:SKSpriteNode,wallRight:SKSpriteNode){
        
        let objectFirstPositionX = spritePositionX
        let playRoomWidth = wallRight.position.x - wallLeft.position.x - wallLeft.size.width/2 - wallRight.size.width/2
        let zeroPosition = wallLeft.position.x + object.size.width/2 + wallLeft.size.width/2+1
        let singleMoveDistance = playRoomWidth/6
        if object.position.x < zeroPosition + playRoomWidth/12{
            object.position.x = zeroPosition
            if abs(object.position.x - objectFirstPositionX!) > 0.5{
                if object.physicsBody?.categoryBitMask != PhysicsCategory.Mouse.rawValue{
                    
                    object.runAction(dragSound)
                    object.physicsBody?.dynamic = false
                    self.moves += 1
                }else{
                    self.moves += 1
                    object.runAction(mouseSound)
                    object.physicsBody?.dynamic = false
                }
            }
        }else{
            
            if object.position.x < zeroPosition + (playRoomWidth/12)*3{
                object.position.x = zeroPosition  + singleMoveDistance
            }else{
                if object.position.x < zeroPosition + (playRoomWidth/12)*5{
                    object.position.x = zeroPosition + singleMoveDistance*2
                }else{
                    if object.position.x < zeroPosition + (playRoomWidth/12)*7{
                        object.position.x = zeroPosition + singleMoveDistance * 3
                    }else{
                        if object.position.x < zeroPosition + (playRoomWidth/12)*9{
                            object.position.x = zeroPosition + singleMoveDistance * 4
                        }
                    }
                    
                }
                
            }
            
        }
        object.physicsBody?.dynamic = false
        if abs(object.position.x - objectFirstPositionX!) > 0.5{
            if object.physicsBody?.categoryBitMask != PhysicsCategory.Mouse.rawValue{
                object.runAction(dragSound)
                self.moves += 1
            }else{
                object.runAction(mouseSound)
                self.moves += 1
            }
        }
    }
    
    func moveVerticaly(object:SKSpriteNode,wallCeiling:SKSpriteNode,wallFloor:SKSpriteNode){
        
        let objectFirstPositionY = spritePositionY
        let playRoomHeight = wallCeiling.position.y - wallFloor.position.y - wallCeiling.size.height/2 - wallFloor.size.height/2
        let zeroPosition = wallCeiling.position.y - object.size.height/2 - wallCeiling.size.height/2-1
        let singleMoveDistance = playRoomHeight/6
        if object.position.y > zeroPosition - playRoomHeight/12{
            object.position.y = zeroPosition
            object.physicsBody?.dynamic = false
            if abs(object.position.y - objectFirstPositionY!) > 0.5{
                    self.moves += 1
                    object.runAction(dragSound)
                }
            
        }else{
            if object.position.y > zeroPosition - (playRoomHeight/12)*3{
                object.position.y = zeroPosition - singleMoveDistance
            }else{
                if object.position.y > zeroPosition - (playRoomHeight/12)*5{
                    object.position.y = zeroPosition - singleMoveDistance*2
                }else{
                    if object.position.y > zeroPosition - (playRoomHeight/12)*7{
                        object.position.y = zeroPosition - singleMoveDistance*3
                    }else {
                        if object.position.y > zeroPosition - (playRoomHeight/12)*9{
                            object.position.y = zeroPosition - singleMoveDistance*4
                        }
                    }
                }
                
            }
            object.physicsBody?.dynamic = false
            if abs(object.position.y - objectFirstPositionY!) > 0.5{
                self.moves += 1
                object.runAction(dragSound)
                
            }
        }
    }
    
    func updateTime(){
        second += 1
        
        if second == 60{
            minute += 1
            second = 0
        }
        if minute == 60{
            hour += 1
            minute = 0
        }
        timeLabel.text = "Time \(hour):\(minute):\(second)"
    }


//    func handlePanFrom(recognizer:UIPanGestureRecognizer){
//        
//        var velocity = recognizer.velocityInView(self.view)
//    
//    }
    
}



    