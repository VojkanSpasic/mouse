//
//  GameOverScene.swift
//  Mouse
//
//  Created by iosakademija on 7/11/16.
//  Copyright © 2016 iosakademija. All rights reserved.
//

import Foundation
import SpriteKit

class GameOverScene: SKScene,SKPhysicsContactDelegate,MouseDelegate {
    
   
    
    var labelRetry : SKLabelNode!
    var labelNextLevel : SKLabelNode!
    var labelMain : SKLabelNode!
    var movesLabel:SKLabelNode!
    
    var levelToLoad = 1
    var level=0
    var moves = 0
    
    func updateMoves(moves: Int) {
        
        self.moves = moves
    }
    
    func updateLevel(level:Int) {
        
        levelToLoad = level+1
        self.level = level
    }
    
    
    
    override func didMoveToView(view: SKView) {
        backgroundColor = SKColor.redColor()
        
        
        labelMain = SKLabelNode(fontNamed: "Chalkduster")
        labelMain.text = "GOOD JOB!"
        labelMain.fontColor = SKColor.greenColor()
        labelMain.fontSize = 40
        labelMain.position = CGPoint(x: frame.size.width/2, y: frame.size.height*0.9)
        addChild(labelMain)
        
        labelRetry = SKLabelNode(fontNamed: "ChalkboardSE-Light")
        labelRetry.color = SKColor.blackColor()
        labelRetry.text = "Retry"
        labelRetry.fontSize = 20
        labelRetry.fontColor = SKColor.blackColor()
        labelRetry.position = CGPoint(x:frame.size.width * 0.25 , y: frame.size.height * 0.1)
        addChild(labelRetry)
        
        labelNextLevel = SKLabelNode(fontNamed: "ChalkboardSE-Light")
        labelNextLevel.text = "Next level"
        labelNextLevel.fontSize = 20
        labelNextLevel.fontColor = SKColor.blackColor()
        labelNextLevel.position = CGPoint(x: frame.size.width*0.75, y: frame.size.height*0.1)
        addChild(labelNextLevel)
        
        movesLabel = SKLabelNode(fontNamed: "ChalkboardSE-Light")
        movesLabel.text = "Moves : \(moves)"
        movesLabel.fontSize = 20
        movesLabel.fontColor = SKColor.yellowColor()
        movesLabel.position = CGPoint(x: frame.size.width*0.5, y: frame.size.height*0.7)
        addChild(movesLabel)
        
    }
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        for touch in touches{
            
            let location = touch.locationInNode(self)
            
            if self.labelRetry.containsPoint(location){
                
                let transition = SKTransition.flipVerticalWithDuration(0.5)
                if let scene = SKScene.sceneWithClassNamed("Level\(level)", fileNamed: "GameScene"){
                    self.view?.presentScene(scene,transition: transition)
                }
            }
            if self.labelNextLevel.containsPoint(location){
                
                let transition = SKTransition.flipVerticalWithDuration(0.5)
                if let scene = SKScene.sceneWithClassNamed("Level\(levelToLoad)", fileNamed: "GameScene"){
                    self.view?.presentScene(scene,transition:transition)
                }
            }
        }
    }
}