//
//  LevelPickerController.swift
//  Mouse
//
//  Created by iosakademija on 7/8/16.
//  Copyright © 2016 iosakademija. All rights reserved.
//

import UIKit

protocol LevelsDelegate{
    
    func updateLevel(level:Int)
}

class LevelPickerController: UITableViewController,LevelsDelegate{
    
    
    var maxLevel = 0

    func updateLevel(level: Int) {
        
        let defaults = NSUserDefaults.standardUserDefaults()
        maxLevel = defaults.integerForKey("highScore")
        if maxLevel<level{
        maxLevel = level
            defaults.setInteger(level, forKey: "highScore")
            tableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let defaults = NSUserDefaults.standardUserDefaults()
        maxLevel = defaults.integerForKey("highScore")
        
        tableView.backgroundColor = UIColor.redColor()
        title = "Levels"
        
        if let font = UIFont(name:"Chalkduster",size:20){
            navigationController?.navigationBar.titleTextAttributes = [NSFontAttributeName:font]
            navigationController?.navigationBar.tintColor = UIColor.redColor()
        
        }
    
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
  
    }

    // MARK: - Table view data source


    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
     
        return maxLevel + 1
    }
    override func prefersStatusBarHidden() -> Bool {
        return true
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("CELL", forIndexPath: indexPath)
        cell.backgroundColor = UIColor.clearColor()
        cell.backgroundColor = UIColor.redColor()
        cell.textLabel?.text = "Level \(indexPath.row+1)"
        return cell
    }
    @IBAction func cancelToLevelPickerController(segue:UIStoryboardSegue){
        
        let defaults = NSUserDefaults.standardUserDefaults()
        maxLevel = defaults.integerForKey("highScore")
        tableView.reloadData()
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let gameViewController = segue.destinationViewController as? GameViewController{
            gameViewController.level = tableView.indexPathForSelectedRow!.row + 1
            
        }
    }
    
}
