//
//  Level9.swift
//  Mouse
//
//  Created by iosakademija on 7/16/16.
//  Copyright © 2016 iosakademija. All rights reserved.
//

import Foundation
import SpriteKit

class Level9:Level{

    override func didMoveToView(view: SKView) {
        super.didMoveToView(view)
        
        level = 9
        
        //dimensions
        let playRoomHeight = wallCeiling.position.y - wallFloor.position.y - wallCeiling.size.height
        let playRoomWidth = wallRight.position.x - wallLeft.position.x - wallRight.size.width
        let yDistance = playRoomHeight/6
        let xDistance = playRoomWidth/6
        
        let boxMiddleHorizontal1 = addMiddleBoxHorizontal(position: CGPointMake(wallLeft.position.x+xDistance*1+wallLeft.size.width/2,
            wallCeiling.position.y-yDistance*1.5),playRoomWidth: playRoomWidth, playRoomHeight: playRoomHeight)
        addChild(boxMiddleHorizontal1)
        
        let boxMiddleHorizontal2 = addMiddleBoxHorizontal(position: CGPointMake(wallLeft.position.x+xDistance*4+wallLeft.size.width/2,
            wallCeiling.position.y-yDistance*0.5),playRoomWidth: playRoomWidth, playRoomHeight: playRoomHeight)
        addChild(boxMiddleHorizontal2)
        
        let boxMiddleHorizontal3 = addMiddleBoxHorizontal(position: CGPointMake(wallLeft.position.x+xDistance*5+wallLeft.size.width/2,
            wallCeiling.position.y-yDistance*3.5),playRoomWidth: playRoomWidth, playRoomHeight: playRoomHeight)
        addChild(boxMiddleHorizontal3)
        
        let boxMiddleHorizontal4 = addMiddleBoxHorizontal(position: CGPointMake(wallLeft.position.x+xDistance*2+wallLeft.size.width/2,
            wallCeiling.position.y-yDistance*4.5),playRoomWidth: playRoomWidth, playRoomHeight: playRoomHeight)
        addChild(boxMiddleHorizontal4)
        
        let boxMiddleHorizontal5 = addMiddleBoxHorizontal(position: CGPointMake(wallLeft.position.x+xDistance*1+wallLeft.size.width/2,
            wallCeiling.position.y-yDistance*5.5),playRoomWidth: playRoomWidth, playRoomHeight: playRoomHeight)
        addChild(boxMiddleHorizontal5)
        
        let boxMiddleHorizontal6 = addMiddleBoxHorizontal(position: CGPointMake(wallLeft.position.x+xDistance*3+wallLeft.size.width/2,
            wallCeiling.position.y-yDistance*5.5),playRoomWidth: playRoomWidth, playRoomHeight: playRoomHeight)
        addChild(boxMiddleHorizontal6)
        
        let boxMiddleVertical1 = addMiddleBoxVertical(position: CGPointMake(wallLeft.position.x+xDistance*2.5+wallLeft.size.width/2,
            wallCeiling.position.y-yDistance*1-wallCeiling.size.height/2),
                                                      playRoomWidth: playRoomWidth, playRoomHeight: playRoomHeight)
        addChild(boxMiddleVertical1)
        
        let boxMiddleVertical2 = addMiddleBoxVertical(position: CGPointMake(wallLeft.position.x+xDistance*4.5+wallLeft.size.width/2,
            wallCeiling.position.y-yDistance*2-wallCeiling.size.height/2),
                                                      playRoomWidth: playRoomWidth, playRoomHeight: playRoomHeight)
        addChild(boxMiddleVertical2)
        
        let boxMiddleVertical3 = addMiddleBoxVertical(position: CGPointMake(wallLeft.position.x+xDistance*5.5+wallLeft.size.width/2,
            wallCeiling.position.y-yDistance*2-wallCeiling.size.height/2),
                                                      playRoomWidth: playRoomWidth, playRoomHeight: playRoomHeight)
        addChild(boxMiddleVertical3)
        
        let boxMiddleVertical4 = addMiddleBoxVertical(position: CGPointMake(wallLeft.position.x+xDistance*4.5+wallLeft.size.width/2,
            wallCeiling.position.y-yDistance*5-wallCeiling.size.height/2),
                                                      playRoomWidth: playRoomWidth, playRoomHeight: playRoomHeight)
        addChild(boxMiddleVertical4)
        
        let boxMiddleVertical5 = addMiddleBoxVertical(position: CGPointMake(wallLeft.position.x+xDistance*5.5+wallLeft.size.width/2,
            wallCeiling.position.y-yDistance*5-wallCeiling.size.height/2),
                                                      playRoomWidth: playRoomWidth, playRoomHeight: playRoomHeight)
        addChild(boxMiddleVertical5)
        
        let boxBigVertical1 = addBigBoxVerticalWith(position: CGPointMake(wallLeft.position.x+xDistance*0.5+wallLeft.size.width/2,
            wallCeiling.position.y-yDistance*3.5-wallCeiling.size.height/2),playRoomWidth: playRoomWidth, playRoomHeight: playRoomHeight)
        addChild(boxBigVertical1)
        
        let boxBigVertical2 = addBigBoxVerticalWith(position: CGPointMake(wallLeft.position.x+xDistance*3.5+wallLeft.size.width/2,
            wallCeiling.position.y-yDistance*3.5-wallCeiling.size.height/2),playRoomWidth: playRoomWidth, playRoomHeight: playRoomHeight)
        addChild(boxBigVertical2)
        
        let mouse = addMouse(position: CGPointMake(wallLeft.position.x+xDistance*2+wallLeft.size.width/2,
            wallCeiling.position.y-yDistance*2.5-wallCeiling.size.height/2),
                             playRoomWidth: playRoomWidth, playRoomHeight: playRoomHeight)
        addChild(mouse)
        
        
        
    }
}
