//
//  GameScene.swift
//  Mouse
//
//  Created by iosakademija on 7/6/16.
//  Copyright (c) 2016 iosakademija. All rights reserved.
//

import SpriteKit





class Level1: Level{
    
    
    
    override func didMoveToView(view: SKView) {
        super.didMoveToView(view)
        
                level = 1

        
        let playRoomHeight = wallCeiling.position.y - wallFloor.position.y - wallCeiling.size.height
        let playRoomWidth = wallRight.position.x - wallLeft.position.x - wallRight.size.width
        let yDistance = playRoomHeight/6
        let xDistance = playRoomWidth/6
        
        let mouse = addMouse(position: CGPointMake(wallLeft.position.x+xDistance*1.5+wallLeft.size.width/2
            ,wallCeiling.position.y-yDistance*4-wallCeiling.size.height/2),
                 playRoomWidth: playRoomWidth,
                 playRoomHeight: playRoomHeight)
        addChild(mouse)
        
        let arrow = SKSpriteNode(imageNamed: "arrow")
        arrow.size = CGSize(width: 100, height: 50)
        arrow.position = CGPointMake(wallRight.position.x-arrow.size.width/2,mouse.position.y)
        arrow.zPosition = -1
        addChild(arrow)
        
        NSNotificationCenter.defaultCenter().postNotificationName("showAlert", object: nil)
       

      
    }

    
}


