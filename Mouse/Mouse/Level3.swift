//
//  Level3.swift
//  Mouse
//
//  Created by iosakademija on 7/13/16.
//  Copyright © 2016 iosakademija. All rights reserved.
//

import UIKit
import SpriteKit

class Level3: Level{
    
    override func didMoveToView(view: SKView) {
        super.didMoveToView(view)
        level = 3
        
        //dimensions
        let playRoomHeight = wallCeiling.position.y - wallFloor.position.y - wallCeiling.size.height
        let playRoomWidth = wallRight.position.x - wallLeft.position.x - wallRight.size.width
        let yDistance = playRoomHeight/6
        let xDistance = playRoomWidth/6
        
        let mouse = addMouse(position: CGPointMake(wallLeft.position.x+xDistance+wallLeft.size.width/2,
            wallCeiling.position.y-yDistance*2.5-wallCeiling.size.height/2),
                             playRoomWidth: playRoomWidth, playRoomHeight: playRoomHeight)
        addChild(mouse)
        let boxMiddleVertical1 = addMiddleBoxVertical(position: CGPointMake(wallLeft.position.x+xDistance/2+wallLeft.size.width/2,wallCeiling.position.y - yDistance-wallCeiling.size.height/2),
                            playRoomWidth: playRoomWidth,
                            playRoomHeight: playRoomHeight)
        addChild(boxMiddleVertical1)
        let boxMiddleHorizontal1 = addMiddleBoxHorizontal(position: CGPointMake(wallLeft.position.x+xDistance*2+wallLeft.size.width/2,wallCeiling.position.y-yDistance*0.5-wallCeiling.size.height/2),
                            playRoomWidth: playRoomWidth, playRoomHeight: playRoomHeight)
        addChild(boxMiddleHorizontal1)
        let boxBigHorizontal = addBigBoxHorizontal(position: CGPointMake(wallLeft.position.x+xDistance*2.5+wallLeft.size.width/2,
            wallCeiling.position.y-yDistance*1.5-wallCeiling.size.height/2),
                                                   playRoomWidth: playRoomWidth, playRoomHeight: playRoomHeight)
        addChild(boxBigHorizontal)
        let boxMiddleVertical2 = addMiddleBoxVertical(position: CGPointMake(wallLeft.position.x+xDistance*4.5+wallLeft.size.width/2,
            wallCeiling.position.y-yDistance-wallCeiling.size.height/2),
                                                    playRoomWidth: playRoomWidth,
                                                    playRoomHeight: playRoomHeight)
        addChild(boxMiddleVertical2)
        
        let boxMiddleVertical3 = addMiddleBoxVertical(position: CGPointMake(wallLeft.position.x+xDistance*5.5+wallLeft.size.width/2,
            wallCeiling.position.y-yDistance*2-wallCeiling.size.height/2),
                                                     playRoomWidth: playRoomWidth, playRoomHeight: playRoomHeight)
        addChild(boxMiddleVertical3)
        
        let boxMiddleVertical4 = addMiddleBoxVertical(position: CGPointMake(wallLeft.position.x+xDistance*2.5+wallLeft.size.width/2,
            wallCeiling.position.y-yDistance*3-wallCeiling.size.height/2),
                                                     playRoomWidth: playRoomWidth, playRoomHeight: playRoomHeight)
        addChild(boxMiddleVertical4)
        
        let boxMiddleVertical5 = addMiddleBoxVertical(position: CGPointMake(wallLeft.position.x+xDistance*3.5+wallLeft.size.width/2,
            wallCeiling.position.y - yDistance*3-wallCeiling.size.height/2),
                                                     playRoomWidth: playRoomWidth, playRoomHeight: playRoomHeight)
        addChild(boxMiddleVertical5)
        
        let boxMiddleVertical6 = addMiddleBoxVertical(position: CGPointMake(wallLeft.position.x+xDistance*0.5+wallLeft.size.width/2,
            wallCeiling.position.y-yDistance*4-wallCeiling.size.height/2),
                                                     playRoomWidth: playRoomWidth, playRoomHeight: playRoomHeight)
        addChild(boxMiddleVertical6)
        
        let boxMiddleVertical7 = addMiddleBoxVertical(position: CGPointMake(wallLeft.position.x+xDistance*2.5+wallLeft.size.width/2,
            wallCeiling.position.y-yDistance*5-wallCeiling.size.height/2),
                                                     playRoomWidth: playRoomWidth, playRoomHeight: playRoomHeight)
        addChild(boxMiddleVertical7)
        let boxMiddleVertical8 = addMiddleBoxVertical(position: CGPointMake(wallLeft.position.x+xDistance*1.5+wallLeft.size.width/2,
            wallCeiling.position.y-yDistance*4+wallCeiling.size.height/2),
                                                     playRoomWidth: playRoomWidth, playRoomHeight: playRoomHeight)
        addChild(boxMiddleVertical8)
        
        let boxMiddleHorizontal2 = addMiddleBoxHorizontal(position: CGPointMake(wallLeft.position.x+xDistance+wallLeft.size.width/2, wallCeiling.position.y-yDistance*5.5-wallCeiling.size.height/2),
                                                          playRoomWidth: playRoomWidth, playRoomHeight: playRoomHeight)
        addChild(boxMiddleHorizontal2)
        
        
        
        
        
        
       
    }
}
