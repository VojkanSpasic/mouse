//
//  Level4.swift
//  Mouse
//
//  Created by iosakademija on 7/14/16.
//  Copyright © 2016 iosakademija. All rights reserved.
//

import Foundation
import SpriteKit

class Level4:Level{

    override func didMoveToView(view: SKView) {
        super.didMoveToView(view)
        level = 4
        
        //dimensions
        let playRoomHeight = wallCeiling.position.y - wallFloor.position.y - wallCeiling.size.height
        let playRoomWidth = wallRight.position.x - wallLeft.position.x - wallRight.size.width
        let yDistance = playRoomHeight/6
        let xDistance = playRoomWidth/6

        let boxMiddleVertical1 = addMiddleBoxVertical(position: CGPointMake(wallLeft.position.x+xDistance*0.5+wallLeft.size.width/2,
            wallCeiling.position.y-yDistance-wallCeiling.size.height/2),
                                                      playRoomWidth: playRoomWidth, playRoomHeight: playRoomHeight)
        addChild(boxMiddleVertical1)
        
        let boxBigHorizontal1 = addBigBoxHorizontal(position: CGPointMake(wallLeft.position.x+xDistance*2.5+wallLeft.size.width/2,
            wallCeiling.position.y-yDistance*1.5-wallCeiling.size.height/2),
                                                   playRoomWidth: playRoomWidth, playRoomHeight: playRoomHeight)
        addChild(boxBigHorizontal1)
        let boxMiddleHorizontal1 = addMiddleBoxHorizontal(position: CGPointMake(wallLeft.position.x+xDistance*2+wallLeft.size.width/2, wallCeiling.position.y-yDistance*0.5-wallCeiling.size.height/2),
                                                          playRoomWidth: playRoomWidth, playRoomHeight: playRoomHeight)
        addChild(boxMiddleHorizontal1)
        
        let boxMiddleHorizontal2 = addMiddleBoxHorizontal(position: CGPointMake(wallLeft.position.x+xDistance*5+wallLeft.size.width/2, wallCeiling.position.y-yDistance*0.5-wallCeiling.size.height/2),
                                                          playRoomWidth: playRoomWidth, playRoomHeight: playRoomHeight)
        addChild(boxMiddleHorizontal2)
        let boxMiddleHorizontal3 = addMiddleBoxHorizontal(position: CGPointMake(wallLeft.position.x+xDistance*5+wallLeft.size.width/2, wallCeiling.position.y-yDistance*1.5-wallCeiling.size.height/2),
                                                          playRoomWidth: playRoomWidth, playRoomHeight: playRoomHeight)
        addChild(boxMiddleHorizontal3)
        let boxMiddleHorizontal4 = addMiddleBoxHorizontal(position: CGPointMake(wallLeft.position.x+xDistance+wallLeft.size.width/2, wallCeiling.position.y-yDistance*3.5-wallCeiling.size.height/2),
                                                          playRoomWidth: playRoomWidth, playRoomHeight: playRoomHeight)
        addChild(boxMiddleHorizontal4)
        let boxMiddleHorizontal5 = addMiddleBoxHorizontal(position: CGPointMake(wallLeft.position.x+xDistance*5+wallLeft.size.width/2, wallCeiling.position.y-yDistance*4.5-wallCeiling.size.height/2),
                                                          playRoomWidth: playRoomWidth, playRoomHeight: playRoomHeight)
        addChild(boxMiddleHorizontal5)
        let boxMiddleVertical2 = addMiddleBoxVertical(position: CGPointMake(wallLeft.position.x+xDistance*3.5+wallLeft.size.width/2,
            wallCeiling.position.y-yDistance*3-wallCeiling.size.height/2),
                                                      playRoomWidth: playRoomWidth, playRoomHeight: playRoomHeight)
        addChild(boxMiddleVertical2)
        let boxMiddleVertical3 = addMiddleBoxVertical(position: CGPointMake(wallLeft.position.x+xDistance*5.5+wallLeft.size.width/2,
            wallCeiling.position.y-yDistance*3-wallCeiling.size.height/2),
                                                      playRoomWidth: playRoomWidth, playRoomHeight: playRoomHeight)
        addChild(boxMiddleVertical3)
        let boxBigHorizontal2 = addBigBoxHorizontal(position: CGPointMake(wallLeft.position.x+xDistance*2.5+wallLeft.size.width/2,
            wallCeiling.position.y-yDistance*4.5-wallCeiling.size.height/2),
                                                    playRoomWidth: playRoomWidth, playRoomHeight: playRoomHeight)
        addChild(boxBigHorizontal2)
        let boxBigHorizontal3 = addBigBoxHorizontal(position: CGPointMake(wallLeft.position.x+xDistance*4.5+wallLeft.size.width/2,
            wallCeiling.position.y-yDistance*5.5-wallCeiling.size.height/2),
                                                    playRoomWidth: playRoomWidth, playRoomHeight: playRoomHeight)
        addChild(boxBigHorizontal3)
        let mouse = addMouse(position: CGPointMake(wallLeft.position.x+xDistance+wallLeft.size.width/2,
            wallCeiling.position.y-yDistance*2.5-wallCeiling.size.height/2),
                             playRoomWidth: playRoomWidth, playRoomHeight: playRoomHeight)
        addChild(mouse)
        
        
        
        
        
        
        

    }

}
