//
//  SKScene++.swift
//  Mouse
//
//  Created by iosakademija on 7/8/16.
//  Copyright © 2016 iosakademija. All rights reserved.
//

import Foundation
import SpriteKit



extension NSObject {
    
    static func sceneWithClassNamed(className: String, fileNamed fileName: String) -> SKScene? {
        let appName = NSBundle.mainBundle().infoDictionary?["CFBundleName"] as! String
        guard let sceneClass = NSClassFromString("\(appName).\(className)") as? SKScene.Type,
            let scene = sceneClass.init(fileNamed: fileName) else {
                return nil
        }
        
        return scene
    }
}






