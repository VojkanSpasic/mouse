//
//  Level2.swift
//  Mouse
//
//  Created by iosakademija on 7/12/16.
//  Copyright © 2016 iosakademija. All rights reserved.
//

import UIKit
import SpriteKit

class Level2: Level{
    
    override func didMoveToView(view: SKView) {
        super.didMoveToView(view)
        
        
      
        level = 2
        let playRoomHeight = wallCeiling.position.y - wallFloor.position.y - wallCeiling.size.height
        let playRoomWidth = wallRight.position.x - wallLeft.position.x - wallRight.size.width
        let yDistance = playRoomHeight/6
        let xDistance = playRoomWidth/6
        

        let bigBox1 = addBigBoxVerticalWith(position: CGPointMake(wallLeft.position.x+xDistance*0.5+wallLeft.size.width/2,
            wallCeiling.position.y - yDistance*3/2-wallCeiling.size.height/2),
                                            playRoomWidth: playRoomWidth,
                                            playRoomHeight: playRoomHeight)
        addChild(bigBox1)
        let bigBox2 = addBigBoxVerticalWith(position: CGPointMake(wallLeft.position.x+xDistance*3.5+wallLeft.size.width/2,
            wallCeiling.position.y - yDistance*3/2-wallCeiling.size.height/2),
                                            playRoomWidth: playRoomWidth,
                                            playRoomHeight: playRoomHeight)
        addChild(bigBox2)
        let mouse = addMouse(position: CGPointMake(wallLeft.position.x+xDistance*2+wallRight.size.width/2,
            wallCeiling.position.y - yDistance*2.5-wallCeiling.size.height/2),
                             playRoomWidth: playRoomWidth,
                             playRoomHeight: playRoomHeight)
        addChild(mouse)

        

        
        // MiddleBox
        let middleBox = SKSpriteNode(imageNamed:"box")
        middleBox.size = CGSize(width: playRoomWidth/6-2,height:playRoomHeight/3-2)
        middleBox.position = CGPoint(x: wallLeft.position.x+xDistance*1.5+wallLeft.size.width/2,
                                     y: wallCeiling.position.y - yDistance - wallCeiling.size.height/2)
        middleBox.physicsBody = SKPhysicsBody(rectangleOfSize: CGSize(width:playRoomWidth/6,height:playRoomHeight/3))
        middleBox.physicsBody?.dynamic = false
        middleBox.physicsBody?.allowsRotation = false
        middleBox.physicsBody?.affectedByGravity = false
        middleBox.physicsBody?.categoryBitMask = PhysicsCategory.Box.rawValue
        
        addChild(middleBox)
        
  
        

        let middleBox2 = SKSpriteNode(imageNamed:"box")
        middleBox2.size = CGSize(width: playRoomWidth/6-2,height:playRoomHeight/3-2)
        middleBox2.position = CGPoint(x: wallLeft.position.x+xDistance*4.5+wallLeft.size.width/2,
                                      y: wallCeiling.position.y - yDistance - wallCeiling.size.height/2)
        middleBox2.physicsBody = SKPhysicsBody(rectangleOfSize: CGSize(width:playRoomWidth/6,height:playRoomHeight/3))
        middleBox2.physicsBody?.dynamic = false
        middleBox2.physicsBody?.allowsRotation = false
        middleBox2.physicsBody?.affectedByGravity = false
        middleBox2.physicsBody?.categoryBitMask = PhysicsCategory.Box.rawValue
        addChild(middleBox2)
        ////
        let middleBox3 = SKSpriteNode(imageNamed:"box")
        middleBox3.size = CGSize(width: playRoomWidth/6-2,height:playRoomHeight/3-2)
        middleBox3.position = CGPoint(x: wallLeft.position.x+xDistance*5.5+wallLeft.size.width/2,
                                      y: wallCeiling.position.y - yDistance - wallCeiling.size.height/2)
        middleBox3.physicsBody = SKPhysicsBody(rectangleOfSize: CGSize(width:playRoomWidth/6,height:playRoomHeight/3))
        middleBox3.physicsBody?.dynamic = false
        middleBox3.physicsBody?.allowsRotation = false
        middleBox3.physicsBody?.affectedByGravity = false
        middleBox3.physicsBody?.categoryBitMask = PhysicsCategory.Box.rawValue
        addChild(middleBox3)
        ////
        let middleBox4 = SKSpriteNode(imageNamed:"box")
        middleBox4.size = CGSize(width: playRoomWidth/6-2,height:playRoomHeight/3-2)
        middleBox4.position = CGPoint(x: wallLeft.position.x+xDistance*2.5+wallLeft.size.width/2,
                                      y: wallCeiling.position.y - yDistance*4 - wallCeiling.size.height/2)
        middleBox4.physicsBody = SKPhysicsBody(rectangleOfSize: CGSize(width:playRoomWidth/6,height:playRoomHeight/3))
        middleBox4.physicsBody?.dynamic = false
        middleBox4.physicsBody?.allowsRotation = false
        middleBox4.physicsBody?.affectedByGravity = false
        middleBox4.physicsBody?.restitution = 0
        middleBox4.physicsBody!.categoryBitMask = PhysicsCategory.Box.rawValue
        addChild(middleBox4)
        ////
        let middleBox5 = SKSpriteNode(imageNamed:"box")
        middleBox5.size = CGSize(width: playRoomWidth/6-2,height:playRoomHeight/3-2)
        middleBox5.position = CGPoint(x: wallLeft.position.x+xDistance*5.5+wallLeft.size.width/2,
                                      y: wallCeiling.position.y - yDistance*5-wallCeiling.size.height/2)
        middleBox5.physicsBody = SKPhysicsBody(rectangleOfSize: CGSize(width:playRoomWidth/6,height:playRoomHeight/3))
        middleBox5.physicsBody?.dynamic = false
        middleBox5.physicsBody?.allowsRotation = false
        middleBox5.physicsBody?.affectedByGravity = false
        middleBox5.physicsBody!.categoryBitMask = PhysicsCategory.Box.rawValue
        addChild(middleBox5)
        //
        let bigBoxHorizontal = SKSpriteNode(imageNamed:"box")
        bigBoxHorizontal.size = CGSize(width:playRoomWidth/2-2,height:playRoomHeight/6 - 2)
        bigBoxHorizontal.position = CGPoint(x:wallLeft.position.x+xDistance*1.5+wallLeft.size.width/2,
                                            y:wallFloor.position.y + yDistance*0.5)
        bigBoxHorizontal.physicsBody = SKPhysicsBody(rectangleOfSize: CGSize(width:playRoomWidth/2,height:playRoomHeight/6))
        bigBoxHorizontal.physicsBody!.dynamic = false
        bigBoxHorizontal.physicsBody!.affectedByGravity = false
        bigBoxHorizontal.physicsBody!.allowsRotation = false
        bigBoxHorizontal.physicsBody!.categoryBitMask = PhysicsCategory.Box.rawValue

        addChild(bigBoxHorizontal)
        //
        let bigBoxHorizontal2 = SKSpriteNode(imageNamed:"box")
        bigBoxHorizontal2.size = CGSize(width:playRoomWidth/2-2,height:playRoomHeight/6 - 2)
        bigBoxHorizontal2.position = CGPoint(x:wallLeft.position.x+xDistance*4.5+wallLeft.size.width/2,
                                             y:wallFloor.position.y + yDistance*2.5)
        bigBoxHorizontal2.physicsBody = SKPhysicsBody(rectangleOfSize: CGSize(width:playRoomWidth/2,height:playRoomHeight/6))
        bigBoxHorizontal2.physicsBody!.dynamic = false
        bigBoxHorizontal2.physicsBody!.affectedByGravity = false
        bigBoxHorizontal2.physicsBody!.allowsRotation = false
        bigBoxHorizontal2.physicsBody?.mass = 1
        bigBoxHorizontal2.physicsBody?.friction = 0
        bigBoxHorizontal2.physicsBody?.angularDamping = 0
        bigBoxHorizontal2.physicsBody?.linearDamping = 0
        bigBoxHorizontal2.physicsBody?.usesPreciseCollisionDetection = true
        bigBoxHorizontal2.physicsBody!.categoryBitMask = PhysicsCategory.Box.rawValue
        addChild(bigBoxHorizontal2)
        
        

    }
        
    
}
